#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import *
from interface import Ui_frmPrincipal
import os,commands

class StartQT4(QtGui.QMainWindow):
        '''
            Esta é uma aplicação python com QT de uma simples interface do youtube-dl
            autor:Sergio Clemente <sergiotucano(at)yahoo.com.br
            modificação: 29/11/2017 13:00
        '''

	def __init__(self, parent=None):
		QtGui.QWidget.__init__(self, parent)
		self.ui = Ui_frmPrincipal()
		self.ui.setupUi(self)		
                # botões de ações da aplicação
                QtCore.QObject.connect(self.ui.btnBaixar,QtCore.SIGNAL("clicked()"), self.baixar)
                QtCore.QObject.connect(self.ui.btnFechar,QtCore.SIGNAL("clicked()"), self.fechar)
                QtCore.QObject.connect(self.ui.btnBusca,QtCore.SIGNAL("clicked()"), self.buscar)
                self.centerOnScreen()
        
        def centerOnScreen(self):
            resolution = QtGui.QDesktopWidget().screenGeometry()
            self.move((resolution.width() / 2) - (self.frameSize().width() / 2),
                  (resolution.height() / 2) - (self.frameSize().height() / 2)) 

	def buscar(self):
		fd = str(QtGui.QFileDialog.getExistingDirectory(self, "Selecionar diretório"))
		self.ui.edtLocal.setText(fd)  	    

	def baixar(self):	    
                       
            vQualidade = unicode(self.ui.cmbQualidade.currentText())
            vLocal = str(self.ui.edtLocal.text())
            vLink  = str(self.ui.edtLink.text())             

            #verificações críticas
            if vLocal != "":
                vLocal = vLocal          
            else:
                QMessageBox.critical(None, "Erro", "Selecione o local para salvar o arquivo.")
                
            if vLink != "":
                vLink = vLink          
            else:
                QMessageBox.critical(None, "Erro", "Selecione um arquivo para baixar.")
                
            if vQualidade == "Baixa":
                vQualidade = " -f worst "
            else:
                vQualidade = " -f best "

            if self.ui.ckbAudio.isChecked():
                vAudio = " -x "
                if vQualidade == "Baixa":
                    vQualidade = " --audio-quality 2 "
                else:
                    vQualidade = " --audio-quality 9 "
            else:
                vAudio = ""

            if self.ui.ckbProxy.isChecked():            
                vProxy = str(" --proxy "+ self.ui.edtProxy.text())
            else:
                vProxy =" "
            
            if self.ui.ckbLegenda.isChecked():
                vLegenda = " --all-subs "
            else:
                vLegenda = " "

            if (vLink != "") and (vLocal != ""):  
                self.ui.txtStatus.setText("Baixando Arquivo")
                app.processEvents()
                strComando = str('youtube-dl -o'+ '"' + vLocal + '/%(title)s-%(id)s.%(ext)s'+ '" '  )            
                strCompleta = strComando + vAudio + vQualidade + vLegenda + vLink + vProxy
                # print strCompleta
                vExec = commands.getoutput(strCompleta)
                self.ui.txtStatus.setText(vExec)        
            
                

        def fechar(self):
            sys.exit(app.exec_())         

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    myapp = StartQT4()
    myapp.show()
    sys.exit(app.exec_())
